/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.service.message;

import ca.uhn.hl7v2.model.v25.message.RRE_O12;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import mx.gnk.hl7.bean.AcuseMsa;
import mx.gnk.hl7.bean.PatientMsh;

/**
 *
 * @author jmejia
 */
public class rreO12message {
    
    private RRE_O12 rre012MessageVal;
    
    public rreO12message(){
        rre012MessageVal = null;
    }
    
    public rreO12message(RRE_O12 message){
       rre012MessageVal=message;
    }    

    public MSH getMshValueRRE012(RRE_O12 message){
        if(message != null){
            return message.getMSH();
        }
        return null;
    }    
    
    public PatientMsh getBeanPatientMshRREO12(RRE_O12 message){
        PatientMsh patientHsh = new PatientMsh();
        if(message != null && message.getMSH()!=null){
            MSH hl7patientMsh = message.getMSH();
            patientHsh.setFieldSeparator(hl7patientMsh.getMsh1_FieldSeparator().getValue());
            patientHsh.setCharacterEncoding(hl7patientMsh.getMsh2_EncodingCharacters().getValue());
            patientHsh.setSistemaEnvio(hl7patientMsh.getMsh3_SendingApplication().getHd1_NamespaceID().getValue());
            patientHsh.setIdUnidadMedica(hl7patientMsh.getMsh4_SendingFacility().getHd1_NamespaceID().getValue());
            patientHsh.setSistemaRecibe(hl7patientMsh.getMsh5_ReceivingApplication().getHd1_NamespaceID().getValue());
            patientHsh.setIdUnidadMedica1(hl7patientMsh.getMsh6_ReceivingFacility().getHd1_NamespaceID().getValue());
            patientHsh.setFecha(hl7patientMsh.getMsh7_DateTimeOfMessage().getTs1_Time().getValue());
            patientHsh.setCodigoMensaje(hl7patientMsh.getMsh9_MessageType().getMsg1_MessageCode().getValue());
            patientHsh.setEventoMensaje(hl7patientMsh.getMsh9_MessageType().getMsg2_TriggerEvent().getValue());
            patientHsh.setEstructuraMensaje(hl7patientMsh.getMsh9_MessageType().getMsg3_MessageStructure().getValue());
            patientHsh.setIdHl7Mensaje(hl7patientMsh.getMsh10_MessageControlID().getValue());
            patientHsh.setIdProceso(hl7patientMsh.getMsh11_ProcessingID().getPt1_ProcessingID().getValue());
            patientHsh.setIdVersion(hl7patientMsh.getMsh12_VersionID().getVid1_VersionID().getValue());
            return patientHsh;
        }
        return null;
    }   
    
    public AcuseMsa getMsaValueRRE012(RRE_O12 message){
        AcuseMsa acuseMsa = new AcuseMsa();
        if(message != null && message.getMSA()!=null){
            MSA hl7acuseMsa = message.getMSA();
            
            acuseMsa.setIdAcuse(hl7acuseMsa.getAcknowledgmentCode().getValue());
            acuseMsa.setIdControlSolicitud(hl7acuseMsa.getMessageControlID().getValue());
            acuseMsa.setDescCondError(hl7acuseMsa.getTextMessage().getValue());
            
            System.out.println("acuseMsa: "+acuseMsa);
            return acuseMsa;
        }
        return null;
    }    
    
}
