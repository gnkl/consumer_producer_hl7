/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.service.message;

import ca.uhn.hl7v2.model.v25.group.RDS_O13_ORDER;
import ca.uhn.hl7v2.model.v25.group.RDS_O13_PATIENT;
import ca.uhn.hl7v2.model.v25.group.RDS_O13_PATIENT_VISIT;
import ca.uhn.hl7v2.model.v25.message.RDS_O13;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.model.v25.segment.ORC;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.RXD;
import ca.uhn.hl7v2.model.v25.segment.RXE;
import mx.gnk.hl7.bean.PatientMsh;
import mx.gnk.hl7.bean.PatientOrc;
import mx.gnk.hl7.bean.PatientPid;
import mx.gnk.hl7.bean.PatientRxd;
import mx.gnk.hl7.bean.PatientRxe;
import mx.gnk.hl7.bean.PatientVisit;
import org.apache.log4j.Logger;

/**
 *
 * @author jmejia
 */
public class rds013message {
    final static Logger logger = Logger.getLogger(rds013message.class);
    
    private RDS_O13 rds013Message;
    
    public rds013message(){
        rds013Message = null;
    }
    
    public rds013message(RDS_O13 message){
       rds013Message=message;
    }
    
    public MSH getMshValueRDS013(RDS_O13 message){
        if(message != null){
            return message.getMSH();
        }
        return null;
    }

    public RDS_O13_PATIENT getInfoPatientRDS013(RDS_O13 message){
        if(message != null){
            return message.getPATIENT();
        }
        return null;
    }

    public PID getInfoPatientPidRDS013(RDS_O13 message){
        if(message != null){
            return message.getPATIENT().getPID();
        }
        return null;
    }

    public RDS_O13_ORDER getOrderRDS013(RDS_O13 message){
        if(message != null){
            return message.getORDER();
        }
        return null;
    }
    
    public PatientMsh getBeanPatientMshRDSO13(RDS_O13 message){
        PatientMsh patientHsh = new PatientMsh();
        if(message != null && message.getMSH()!=null){
            MSH hl7patientMsh = message.getMSH();
            patientHsh.setFieldSeparator(hl7patientMsh.getMsh1_FieldSeparator().getValue());
            patientHsh.setCharacterEncoding(hl7patientMsh.getMsh2_EncodingCharacters().getValue());
            patientHsh.setSistemaEnvio(hl7patientMsh.getMsh3_SendingApplication().getHd1_NamespaceID().getValue());
            patientHsh.setIdUnidadMedica(hl7patientMsh.getMsh4_SendingFacility().getHd1_NamespaceID().getValue());
            patientHsh.setSistemaRecibe(hl7patientMsh.getMsh5_ReceivingApplication().getHd1_NamespaceID().getValue());
            patientHsh.setIdUnidadMedica1(hl7patientMsh.getMsh6_ReceivingFacility().getHd1_NamespaceID().getValue());
            patientHsh.setFecha(hl7patientMsh.getMsh7_DateTimeOfMessage().getTs1_Time().getValue());
            patientHsh.setCodigoMensaje(hl7patientMsh.getMsh9_MessageType().getMsg1_MessageCode().getValue());
            patientHsh.setEventoMensaje(hl7patientMsh.getMsh9_MessageType().getMsg2_TriggerEvent().getValue());
            patientHsh.setEstructuraMensaje(hl7patientMsh.getMsh9_MessageType().getMsg3_MessageStructure().getValue());
            patientHsh.setIdHl7Mensaje(hl7patientMsh.getMsh10_MessageControlID().getValue());
            patientHsh.setIdProceso(hl7patientMsh.getMsh11_ProcessingID().getPt1_ProcessingID().getValue());
            patientHsh.setIdVersion(hl7patientMsh.getMsh12_VersionID().getVid1_VersionID().getValue());
            return patientHsh;
        }
        return null;
    }    
    
    
    public PatientPid getBeanPatientRDS013(RDS_O13 message) throws NullPointerException, NumberFormatException{
        PatientPid patient = new PatientPid();
        if(message != null && message.getPATIENT()!=null){
            PID hl7patientNode = message.getPATIENT().getPID();
            
            patient.setIdSegmento(Long.valueOf(hl7patientNode.getPid1_SetIDPID().getValue()));
            patient.setIdPaciente(Long.valueOf(hl7patientNode.getPid3_PatientIdentifierList(0).getCx1_IDNumber().getValue()));
            patient.setAssignAutoridad(hl7patientNode.getPid3_PatientIdentifierList(0).getCx4_AssigningAuthority().getHd1_NamespaceID().getValue());
            patient.setTipoCodigo(hl7patientNode.getPid3_PatientIdentifierList(0).getCx5_IdentifierTypeCode().getValue());
            
            patient.setCurp(hl7patientNode.getPid3_PatientIdentifierList(1).getCx1_IDNumber().getValue());
            patient.setJurisdiccion(hl7patientNode.getPid3_PatientIdentifierList(1).getCx4_AssigningAuthority().getHd1_NamespaceID().getValue());
            patient.setAgenciaDepto(hl7patientNode.getPid3_PatientIdentifierList(1).getCx5_IdentifierTypeCode().getValue());
            
            patient.setApPaterno(hl7patientNode.getPid5_PatientName(0).getXpn1_FamilyName().getFn1_Surname().getValue());
            patient.setApMaterno(hl7patientNode.getPid5_PatientName(0).getXpn1_FamilyName().getFn2_OwnSurnamePrefix().getValue());
            patient.setPrimerNombre(hl7patientNode.getPid5_PatientName(0).getXpn2_GivenName().getValue());
            patient.setSegundoNombre(hl7patientNode.getPid5_PatientName(0).getXpn3_SecondAndFurtherGivenNamesOrInitialsThereof().getValue());
            
            patient.setFechaNacimiento(hl7patientNode.getPid7_DateTimeOfBirth().getTs1_Time().getValue());
            patient.setSexo(hl7patientNode.getPid8_AdministrativeSex().getValue());
            
            System.out.println("patient: "+patient);
            return patient;
            
        }
        return null;
    }
    
    //throws NullPointerException, NumberFormatException
    public PatientVisit getBeanVisitPatientRDS013(RDS_O13 message) {
        PatientVisit patientVisit = new PatientVisit();
        if(message != null && message.getPATIENT()!=null){
           try{
                RDS_O13_PATIENT_VISIT hl7patientVisit = message.getPATIENT().getPATIENT_VISIT();
                patientVisit.setIdSegmento(Long.valueOf(hl7patientVisit.getPV1().getSetIDPV1().getValue()));
                patientVisit.setViaAdmon(hl7patientVisit.getPV1().getPv12_PatientClass().getValue());
                
                String pointOfCare = hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl1_PointOfCare().getValue();
                Long pointOfCareNumber = pointOfCare!=null&&!pointOfCare.isEmpty()?Long.valueOf(pointOfCare):null;
                patientVisit.setPuntoAtencion(pointOfCareNumber);

                String room = hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl2_Room().getValue();
                Long roomNumber = room!=null&&!room.isEmpty()?Long.valueOf(room):null;
                
                patientVisit.setCuarto(roomNumber);
                if(hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl3_Bed().getValue() != null){
                    patientVisit.setCama(hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl3_Bed().getValue());
                }
                patientVisit.setUnidadMedica(hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl4_Facility().getHd1_NamespaceID().getValue());

                String numVisita = hl7patientVisit.getPV1().getPv119_VisitNumber().getCx1_IDNumber().getValue();
                Long numVisitaNum = numVisita!=null&&!numVisita.isEmpty()?Long.valueOf(numVisita):null;
                
                patientVisit.setNumeroVisita(numVisitaNum);
                patientVisit.setFechaSolicitud(hl7patientVisit.getPV1().getPv144_AdmitDateTime().getTs1_Time().getValue());                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            
            System.out.println("patient visit: "+patientVisit);
            
            return patientVisit;
        }
        return null;
    }

    public PatientOrc getBeanOrderOrcRDS013(RDS_O13 message) {
        PatientOrc patientOrc = new PatientOrc();
        if(message != null && message.getPATIENT()!=null){
           try{
                ORC hl7patientOrc = message.getORDER().getORC();            
                patientOrc.setOrderControl(hl7patientOrc.getOrc1_OrderControl().getValue());
                patientOrc.setIdReceta(hl7patientOrc.getOrc2_PlacerOrderNumber().getEi1_EntityIdentifier().getValue());
                patientOrc.setIdUnidadMedica(hl7patientOrc.getOrc2_PlacerOrderNumber().getEi2_NamespaceID().getValue());
                patientOrc.setIdSolicitud(hl7patientOrc.getOrc4_PlacerGroupNumber().getEi1_EntityIdentifier().getValue());
                patientOrc.setIdUnidadMedicaGroup(hl7patientOrc.getOrc4_PlacerGroupNumber().getEi2_NamespaceID().getValue());
                patientOrc.setFecha(hl7patientOrc.getOrc9_DateTimeOfTransaction().getTs1_Time().getValue());                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception ",ex);
           }
            
            System.out.println("patientOrc: "+patientOrc);
            
            return patientOrc;
        }
        return null;
    }
    
    
    public PatientRxe getBeanOrderRxeRDS013(RDS_O13 message) {
        PatientRxe patientRxe = new PatientRxe();
        if(message != null && message.getPATIENT()!=null){
           try{        
                RXE hl7patientRxe = message.getORDER().getENCODING().getRXE();                
                if(hl7patientRxe.getRxe1_QuantityTiming().getTq1_Quantity().getCq1_Quantity().getValue()!=null){
                    patientRxe.setCantidadMedicamento(Long.valueOf(hl7patientRxe.getRxe1_QuantityTiming().getTq1_Quantity().getCq1_Quantity().getValue()));
                }
                String intRep = hl7patientRxe.getRxe1_QuantityTiming().getTq2_Interval().getRi1_RepeatPattern().getValue();
                patientRxe.setIntervaloRepeticion(intRep!=null?intRep:"");
                
                String intTeExpl = hl7patientRxe.getRxe1_QuantityTiming().getTq2_Interval().getRi2_ExplicitTimeInterval().getValue();
                patientRxe.setIntervaloTiempoExplicito(intTeExpl!=null?intTeExpl:"");
                
                String durTrat = hl7patientRxe.getRxe1_QuantityTiming().getDuration().getValue();
                patientRxe.setDuracionTratamiento(durTrat!=null?Integer.valueOf(durTrat):0);
                
                String fechaIniTrat = hl7patientRxe.getRxe1_QuantityTiming().getStartDateTime().getTs1_Time().getValue();
                patientRxe.setFechaIniPrescripcion(fechaIniTrat);

                String fechaFinTrat = hl7patientRxe.getRxe1_QuantityTiming().getEndDateTime().getTs1_Time().getValue();
                patientRxe.setFechaFinPrescripcion(fechaFinTrat);
                
                String condiciones = hl7patientRxe.getRxe1_QuantityTiming().getCondition().getValue();
                patientRxe.setCondicionesUso(condiciones);
                
                String texto = hl7patientRxe.getRxe1_QuantityTiming().getText().getValue();
                patientRxe.setTextoAdicional(texto);
                
                String idmedicamento = hl7patientRxe.getGiveCode().getIdentifier().getValue();
                patientRxe.setIdMedicamento(idmedicamento);
                
                String descmed = hl7patientRxe.getGiveCode().getText().getValue();
                patientRxe.setDescMedicamento(descmed);

                String nomCod = hl7patientRxe.getGiveCode().getNameOfCodingSystem().getValue();
                patientRxe.setCodigoMedicamento(nomCod);
                
                String cantmin = hl7patientRxe.getGiveAmountMinimum().getValue();
                patientRxe.setCantMinEntrega(cantmin);
                
                String cantmax = hl7patientRxe.getGiveAmountMaximum().getValue();
                patientRxe.setCantMaxEntrega(cantmax);
                
                String uniMed = hl7patientRxe.getGiveUnits().getCe2_Text().getValue();
                patientRxe.setUnidadMedida(uniMed);
                
                String dosis = hl7patientRxe.getGiveDosageForm().getCe2_Text().getValue();
                patientRxe.setDosis(dosis);
                
                String instrucciones = hl7patientRxe.getProviderSAdministrationInstructions(0).getCe2_Text().getValue();
                patientRxe.setInstrucciones(instrucciones);
                
                String disAmn = hl7patientRxe.getDispenseAmount().getValue();
                patientRxe.setCantidadAdministrar(Long.valueOf(disAmn));
                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception ",ex);
           }
            
            System.out.println("patientRxe: "+patientRxe);
            
            return patientRxe;
        }
        return null;
    }    

    public PatientRxd getBeanOrderRxdRDS013(RDS_O13 message) {
        PatientRxd patientRxd = new PatientRxd();
        if(message != null && message.getPATIENT()!=null){
           try{        
           
                RXD hl7patientRxd = message.getORDER().getRXD();
                
                if(hl7patientRxd.getDispenseSubIDCounter().getValue()!=null){
                    patientRxd.setContador(Long.valueOf(hl7patientRxd.getDispenseSubIDCounter().getValue()));
                    //patientRxd.setCantidadMedicamento(Long.valueOf(hl7patientRxd.getDispenseSubIDCounter().getValue()));
                }
                if(hl7patientRxd.getDispenseGiveCode().getCe1_Identifier().getValue() !=null){
                    patientRxd.setCodigoMedicamento(hl7patientRxd.getDispenseGiveCode().getCe1_Identifier().getValue());
                }
                if(hl7patientRxd.getDispenseGiveCode().getCe2_Text().getValue() !=null){
                    patientRxd.setDescripcionMedicamento(hl7patientRxd.getDispenseGiveCode().getCe2_Text().getValue());
                }

                if(hl7patientRxd.getDateTimeDispensed().getTs1_Time().getValue() !=null){
                    patientRxd.setFechaEntrega(hl7patientRxd.getDateTimeDispensed().getTs1_Time().getValue());
                }

                if(hl7patientRxd.getActualDispenseAmount().getValue() !=null){
                    patientRxd.setCantidadMedicamento(Long.valueOf(hl7patientRxd.getActualDispenseAmount().getValue()));
                }

                if(hl7patientRxd.getActualDispenseUnits().getCe2_Text().getValue()  !=null){
                    patientRxd.setPresentacionMedicamento(hl7patientRxd.getActualDispenseUnits().getCe2_Text().getValue());
                }

                if(hl7patientRxd.getPrescriptionNumber().getValue()!=null){
                    patientRxd.setNumeroPrescripcion(hl7patientRxd.getPrescriptionNumber().getValue());
                }

                if(hl7patientRxd.getDispenseType().getCwe1_Identifier().getValue()!=null){
                    patientRxd.setTipoEntrega(hl7patientRxd.getDispenseType().getCwe1_Identifier().getValue());
                }                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception ",ex);
           }
            
            System.out.println("patientRxe: "+patientRxd);
            
            return patientRxd;
        }
        return null;
    }    
    
    
}
