/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.service.message;

import ca.uhn.hl7v2.model.v25.datatype.XCN;
import ca.uhn.hl7v2.model.v25.group.RDE_O11_PATIENT_VISIT;
import ca.uhn.hl7v2.model.v25.message.RDE_O11;
import ca.uhn.hl7v2.model.v25.segment.FT1;
import ca.uhn.hl7v2.model.v25.segment.MSH;
import ca.uhn.hl7v2.model.v25.segment.NTE;
import ca.uhn.hl7v2.model.v25.segment.ORC;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.RXC;
import ca.uhn.hl7v2.model.v25.segment.RXE;
import ca.uhn.hl7v2.model.v25.segment.RXR;
import ca.uhn.hl7v2.model.v25.segment.TQ1;
import mx.gnk.hl7.bean.PatientFt1;
import mx.gnk.hl7.bean.PatientMsh;
import mx.gnk.hl7.bean.PatientNte;
import mx.gnk.hl7.bean.PatientOrc;
import mx.gnk.hl7.bean.PatientPid;
import mx.gnk.hl7.bean.PatientRxc;
import mx.gnk.hl7.bean.PatientRxe;
import mx.gnk.hl7.bean.PatientRxr;
import mx.gnk.hl7.bean.PatientTq1;
import mx.gnk.hl7.bean.PatientVisit;
import org.apache.log4j.Logger;

/**
 *
 * @author jmejia
 */
public class rde011message {
    final static Logger logger = Logger.getLogger(rde011message.class);

    private RDE_O11 rds011MessageVal;
    
    public rde011message(){
        rds011MessageVal = null;
    }
    
    public rde011message(RDE_O11 message){
       rds011MessageVal=message;
    }    

    public MSH getMshValueRDS013(RDE_O11 message){
        if(message != null){
            return message.getMSH();
        }
        return null;
    }

    public PatientMsh getBeanPatientMshRDE011(RDE_O11 message){
        PatientMsh patientHsh = new PatientMsh();
        if(message != null && message.getMSH()!=null){
            MSH hl7patientMsh = message.getMSH();
            patientHsh.setFieldSeparator(hl7patientMsh.getMsh1_FieldSeparator().getValue());
            patientHsh.setCharacterEncoding(hl7patientMsh.getMsh2_EncodingCharacters().getValue());
            patientHsh.setSistemaEnvio(hl7patientMsh.getMsh3_SendingApplication().getHd1_NamespaceID().getValue());
            patientHsh.setIdUnidadMedica(hl7patientMsh.getMsh4_SendingFacility().getHd1_NamespaceID().getValue());
            patientHsh.setSistemaRecibe(hl7patientMsh.getMsh5_ReceivingApplication().getHd1_NamespaceID().getValue());
            patientHsh.setIdUnidadMedica1(hl7patientMsh.getMsh6_ReceivingFacility().getHd1_NamespaceID().getValue());
            patientHsh.setFecha(hl7patientMsh.getMsh7_DateTimeOfMessage().getTs1_Time().getValue());
            patientHsh.setCodigoMensaje(hl7patientMsh.getMsh9_MessageType().getMsg1_MessageCode().getValue());
            patientHsh.setEventoMensaje(hl7patientMsh.getMsh9_MessageType().getMsg2_TriggerEvent().getValue());
            patientHsh.setEstructuraMensaje(hl7patientMsh.getMsh9_MessageType().getMsg3_MessageStructure().getValue());
            patientHsh.setIdHl7Mensaje(hl7patientMsh.getMsh10_MessageControlID().getValue());
            patientHsh.setIdProceso(hl7patientMsh.getMsh11_ProcessingID().getPt1_ProcessingID().getValue());
            patientHsh.setIdVersion(hl7patientMsh.getMsh12_VersionID().getVid1_VersionID().getValue());
            patientHsh.setAcceptAckType(hl7patientMsh.getMsh15_AcceptAcknowledgmentType().getValue());
            patientHsh.setAppAckType(hl7patientMsh.getMsh16_ApplicationAcknowledgmentType().getValue());
            return patientHsh;
        }
        return null;
    }
    

    public PatientPid getBeanPatientRDE011(RDE_O11 message) throws NullPointerException, NumberFormatException{
        PatientPid patient = new PatientPid();
        if(message != null && message.getPATIENT()!=null){
            PID hl7patientNode = message.getPATIENT().getPID();
            
            
            patient.setIdSegmento(Long.valueOf(hl7patientNode.getPid1_SetIDPID().getValue()));            
            patient.setIdPaciente(Long.valueOf(hl7patientNode.getPid3_PatientIdentifierList(0).getCx1_IDNumber().getValue()));
            patient.setAssignAutoridad(hl7patientNode.getPid3_PatientIdentifierList(0).getCx4_AssigningAuthority().getHd1_NamespaceID().getValue());
            patient.setTipoCodigo(hl7patientNode.getPid3_PatientIdentifierList(0).getCx5_IdentifierTypeCode().getValue());
            
            patient.setCurp(hl7patientNode.getPid3_PatientIdentifierList(0).getCx6_AssigningFacility().getHd1_NamespaceID().getValue());
            patient.setJurisdiccion(hl7patientNode.getPid3_PatientIdentifierList(0).getCx9_AssigningJurisdiction().getCwe2_Text().getValue());
            patient.setAgenciaDepto(hl7patientNode.getPid3_PatientIdentifierList(0).getCx10_AssigningAgencyOrDepartment().getCwe2_Text().getValue());
            
            patient.setApPaterno(hl7patientNode.getPid5_PatientName(0).getXpn1_FamilyName().getFn1_Surname().getValue());
            patient.setApMaterno(hl7patientNode.getPid5_PatientName(0).getXpn1_FamilyName().getFn2_OwnSurnamePrefix().getValue());
            patient.setPrimerNombre(hl7patientNode.getPid5_PatientName(0).getXpn2_GivenName().getValue());
            patient.setSegundoNombre(hl7patientNode.getPid5_PatientName(0).getXpn3_SecondAndFurtherGivenNamesOrInitialsThereof().getValue());
            
            patient.setFechaNacimiento(hl7patientNode.getPid7_DateTimeOfBirth().getTs1_Time().getValue());
            patient.setSexo(hl7patientNode.getPid8_AdministrativeSex().getValue());
            
            System.out.println("patient rde: "+patient);
            return patient;
            
        }
        return null;
    }
    
    public PatientVisit getBeanVisitPatientRDE011(RDE_O11 message) {
        PatientVisit patientVisit = new PatientVisit();
        if(message != null && message.getPATIENT()!=null){
           try{
                RDE_O11_PATIENT_VISIT hl7patientVisit = message.getPATIENT().getPATIENT_VISIT();
                
                patientVisit.setIdSegmento(Long.valueOf(hl7patientVisit.getPV1().getSetIDPV1().getValue()));
                patientVisit.setViaAdmon(hl7patientVisit.getPV1().getPv12_PatientClass().getValue());
                patientVisit.setPuntoAtencion(Long.valueOf(hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl1_PointOfCare().getValue()));
                patientVisit.setCuarto(Long.valueOf(hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl2_Room().getValue()));                
                patientVisit.setUnidadMedica(hl7patientVisit.getPV1().getPv13_AssignedPatientLocation().getPl4_Facility().getHd1_NamespaceID().getValue());
                patientVisit.setNumeroVisita(Long.valueOf(hl7patientVisit.getPV1().getPv119_VisitNumber().getCx1_IDNumber().getValue()));
                //TODO
                // identificador persona
                // codigo tipo identificador
                //identificador de unidad medica
                patientVisit.setIdPersona(hl7patientVisit.getPV1().getPv119_VisitNumber().getCx4_AssigningAuthority().getHd1_NamespaceID().getValue());
                patientVisit.setTipoCodigo(hl7patientVisit.getPV1().getPv119_VisitNumber().getCx5_IdentifierTypeCode().getValue());
                patientVisit.setIdUnidadMedica(hl7patientVisit.getPV1().getPv119_VisitNumber().getCx6_AssigningFacility().getHd1_NamespaceID().getValue());                
                patientVisit.setFechaSolicitud(hl7patientVisit.getPV1().getPv144_AdmitDateTime().getTs1_Time().getValue());                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            
            System.out.println("patient visit: "+patientVisit);
            
            return patientVisit;
        }
        return null;
    }

    public PatientOrc getBeanOrderOrcRDE011(RDE_O11 message) {
        PatientOrc patientOrc = new PatientOrc();
        if(message != null && message.getPATIENT()!=null){
           try{
                ORC hl7patientOrc = message.getORDER().getORC();
                
                patientOrc.setOrderControl(hl7patientOrc.getOrc1_OrderControl().getValue());
                patientOrc.setIdReceta(hl7patientOrc.getOrc2_PlacerOrderNumber().getEi1_EntityIdentifier().getValue());
                patientOrc.setSistemaId(hl7patientOrc.getOrc2_PlacerOrderNumber().getEi2_NamespaceID().getValue());
                patientOrc.setGroupNumberId(hl7patientOrc.getOrc4_PlacerGroupNumber().getEi1_EntityIdentifier().getValue());
                patientOrc.setIdSolicitud(hl7patientOrc.getOrc4_PlacerGroupNumber().getEi2_NamespaceID().getValue());
                patientOrc.setFecha(hl7patientOrc.getOrc9_DateTimeOfTransaction().getTs1_Time().getValue());                     
                patientOrc.setProveedorOrden(hl7patientOrc.getOrc12_OrderingProvider(0).getXcn1_IDNumber().getValue());
                patientOrc.setApellidoPaterno(hl7patientOrc.getOrc12_OrderingProvider(0).getXcn2_FamilyName().getFn1_Surname().getValue());
                patientOrc.setApellidoMaterno(hl7patientOrc.getOrc12_OrderingProvider(0).getXcn2_FamilyName().getFn2_OwnSurnamePrefix().getValue());
                patientOrc.setPrimerNombre(hl7patientOrc.getOrc12_OrderingProvider(0).getXcn3_GivenName().getValue());
                patientOrc.setSegundoNombre(hl7patientOrc.getOrc12_OrderingProvider(0).getXcn4_SecondAndFurtherGivenNamesOrInitialsThereof().getValue());
                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            
            System.out.println("patientOrc: "+patientOrc);
            
            return patientOrc;
        }
        return null;
    }
    
    
    public PatientRxe getBeanOrderRxeRDE011(RDE_O11 message) {
        PatientRxe patientRxe = new PatientRxe();
        if(message != null && message.getPATIENT()!=null){
           try{        
                RXE hl7patientRxe = message.getORDER().getRXE();
                
                String cantMedica = hl7patientRxe.getRxe1_QuantityTiming().getTq1_Quantity().getCq1_Quantity().getValue();
                double value = Double.parseDouble(cantMedica);
                long x = (long) value;
                
                patientRxe.setCantidadMedicamento(cantMedica!=null?Long.valueOf(x):0);

                //TODO no se encuentra RepeatInterval
                String intRep = hl7patientRxe.getRxe1_QuantityTiming().getTq2_Interval().getRi1_RepeatPattern().getValue();
                patientRxe.setIntervaloRepeticion(intRep!=null?intRep:"");
                //TODO
                String intTeExpl = hl7patientRxe.getRxe1_QuantityTiming().getTq2_Interval().getRi2_ExplicitTimeInterval().getValue();
                patientRxe.setIntervaloTiempoExplicito(intTeExpl!=null?intTeExpl:"");
                
                String durTrat = hl7patientRxe.getRxe1_QuantityTiming().getTq3_Duration().getValue();
                patientRxe.setDuracionTratamiento(durTrat!=null?Integer.valueOf(durTrat):0);
                
                String fechaIniTrat = hl7patientRxe.getRxe1_QuantityTiming().getTq4_StartDateTime().getTs1_Time().getValue();
                patientRxe.setFechaIniPrescripcion(fechaIniTrat);

                String fechaFinTrat = hl7patientRxe.getRxe1_QuantityTiming().getTq5_EndDateTime().getTs1_Time().getValue();
                patientRxe.setFechaFinPrescripcion(fechaFinTrat);
                
                String condiciones = hl7patientRxe.getRxe1_QuantityTiming().getCondition().getValue();
                patientRxe.setCondicionesUso(condiciones);
                
                String texto = hl7patientRxe.getRxe1_QuantityTiming().getText().getValue();
                patientRxe.setTextoAdicional(texto);
                
                String idmedicamento = hl7patientRxe.getRxe2_GiveCode().getCe1_Identifier().getValue();
                patientRxe.setIdMedicamento(idmedicamento);
                
                String descmed = hl7patientRxe.getRxe2_GiveCode().getCe2_Text().getValue();
                patientRxe.setDescMedicamento(descmed);

                String nomCod = hl7patientRxe.getRxe2_GiveCode().getCe3_NameOfCodingSystem().getValue();
                patientRxe.setCodigoMedicamento(nomCod);
                
                String cantmin = hl7patientRxe.getRxe3_GiveAmountMinimum().getValue();
                patientRxe.setCantMinEntrega(cantmin);
                
                String cantmax = hl7patientRxe.getRxe4_GiveAmountMaximum().getValue();
                patientRxe.setCantMaxEntrega(cantmax);
                
                String uniMed = hl7patientRxe.getRxe5_GiveUnits().getCe2_Text().getValue();
                patientRxe.setUnidadMedida(uniMed);
                
                String dosis = hl7patientRxe.getRxe6_GiveDosageForm().getCe2_Text().getValue();
                patientRxe.setDosis(dosis);
                
                String instrucciones = hl7patientRxe.getRxe7_ProviderSAdministrationInstructions(0).getCe2_Text().getValue();
                patientRxe.setInstrucciones(instrucciones);
                
                String disAmn = hl7patientRxe.getRxe10_DispenseAmount().getValue();
                patientRxe.setCantidadAdministrar(disAmn!=null?Long.valueOf(disAmn):0);
                
                XCN test = hl7patientRxe.getRxe13_OrderingProviderSDEANumber(0);
                String total = test.getXcn1_IDNumber().getValue();
                patientRxe.setCantidadTotal(total);
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            
            System.out.println("patientRxe: "+patientRxe);
            
            return patientRxe;
        }
        return null;
    }

    public PatientNte getBeanOrderNteRDE011(RDE_O11 message) {
        PatientNte patientNte = new PatientNte();
        if(message != null && message.getPATIENT()!=null){
           try{
                NTE hl7patientNte = message.getORDER().getNTE();
                
                patientNte.setIdSegmentoNte(hl7patientNte.getNte1_SetIDNTE().getValue());
                patientNte.setComment(hl7patientNte.getNte3_Comment(0).getValue());                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            System.out.println("PatientNte: "+patientNte);
            return patientNte;
        }
        return null;
        
    }
    
    public PatientTq1 getBeanOrderTq1RDE011(RDE_O11 message) {
        PatientTq1 patientTq1 = new PatientTq1();
        if(message != null && message.getPATIENT()!=null){
           try{
                TQ1 hl7patientTq1 = message.getORDER().getTIMING().getTQ1();
                
                patientTq1.setIdTq1(hl7patientTq1.getSetIDTQ1().getValue());
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            System.out.println("PatientNte: "+patientTq1);
            return patientTq1;
        }
        return null;
        
    }
    

    public PatientRxr getBeanOrderRxrRDE011(RDE_O11 message) {
        PatientRxr patientRxr = new PatientRxr();
        if(message != null && message.getPATIENT()!=null){
           try{
                RXR hl7patientRxr = message.getORDER().getRXR();
                
                patientRxr.setCodigoViaAdmon(hl7patientRxr.getRxr1_Route().getCe1_Identifier().getValue());
                patientRxr.setDescripcionSitioAdmon(hl7patientRxr.getRxr1_Route().getCe2_Text().getValue());
                patientRxr.setIdAdmonCodificacion(hl7patientRxr.getRxr1_Route().getCe3_NameOfCodingSystem().getValue());
                patientRxr.setIdSitioAdmonTratamiento(hl7patientRxr.getRxr2_AdministrationSite().getCwe1_Identifier().getValue());
                patientRxr.setDescripcionSitioAdmon(hl7patientRxr.getRxr2_AdministrationSite().getCwe2_Text().getValue());
                patientRxr.setIdSitioAdmonCodificacion(hl7patientRxr.getRxr2_AdministrationSite().getCwe3_NameOfCodingSystem().getValue());
                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            System.out.println("patientRxr: "+patientRxr);
            return patientRxr;
        }
        return null;
        
    }

    public PatientRxc getBeanOrderRxcRDE011(RDE_O11 message) {
        PatientRxc patientRxc = new PatientRxc();
        if(message != null && message.getPATIENT()!=null){
           try{
                RXC hl7patientRxc = message.getORDER().getRXC();
                patientRxc.setRxComponentType(hl7patientRxc.getRxc1_RXComponentType().getValue());
                patientRxc.setIdComponente(hl7patientRxc.getRxc2_ComponentCode().getCe1_Identifier().getValue());
                patientRxc.setDescripcionComponente(hl7patientRxc.getRxc2_ComponentCode().getCe2_Text().getValue());
                patientRxc.setIdSistemaCodificacion(hl7patientRxc.getRxc2_ComponentCode().getCe3_NameOfCodingSystem().getValue());
                patientRxc.setCantidadComprobante(hl7patientRxc.getRxc3_ComponentAmount().getValue());
                patientRxc.setIdUnidadMedida(hl7patientRxc.getRxc4_ComponentUnits().getCe1_Identifier().getValue());
                patientRxc.setDescripcionUnidadMedida(hl7patientRxc.getRxc4_ComponentUnits().getCe2_Text().getValue());                
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            System.out.println("patientRxr: "+patientRxc);
            return patientRxc;
        }
        return null;
        
    }

    public PatientFt1 getBeanOrderFt1RDE011(RDE_O11 message) {
        PatientFt1 patientFt1 = new PatientFt1();
        if(message != null && message.getPATIENT()!=null){
           try{
                FT1 hl7patientFt1 = message.getORDER().getFT1();
                patientFt1.setIdSegmentoFt1(hl7patientFt1.getFt11_SetIDFT1().getValue());
                patientFt1.setFechaSolicitud(hl7patientFt1.getFt14_TransactionDate().getDr1_RangeStartDateTime().getTs1_Time().getValue());
                patientFt1.setTipoTransaccion(hl7patientFt1.getFt16_TransactionType().getValue());
                patientFt1.setIdCodigoTransaccion(hl7patientFt1.getFt17_TransactionCode().getCe1_Identifier().getValue());
                patientFt1.setCodigoDiagnostico(hl7patientFt1.getFt119_DiagnosisCodeFT1(0).getCe1_Identifier().getValue());
                patientFt1.setNombreCodigoDiagnostico(hl7patientFt1.getDiagnosisCodeFT1(0).getCe2_Text().getValue());
           }catch(Exception ex ){
               //ex.printStackTrace();
               logger.error("Exception: ",ex);
           }
            System.out.println("patientRxr: "+patientFt1);
            return patientFt1;
        }
        return null;   
    }
    
    
    /**
     * @return the rds011MessageVal
     */
    public RDE_O11 getRds011MessageVal() {
        return rds011MessageVal;
    }

    /**
     * @param rds011MessageVal the rds011MessageVal to set
     */
    public void setRds011MessageVal(RDE_O11 rds011MessageVal) {
        this.rds011MessageVal = rds011MessageVal;
    }
    
}
