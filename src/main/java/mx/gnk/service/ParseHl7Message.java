/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnk.service;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.RDE_O11;
import ca.uhn.hl7v2.model.v25.message.RDS_O13;
import ca.uhn.hl7v2.model.v25.message.RRE_O12;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.XMLParser;
import java.io.Serializable;
import mx.gnk.hl7.bean.AcuseMsa;
import mx.gnk.hl7.bean.Hl7Message;
import mx.gnk.hl7.bean.PatientFt1;
import mx.gnk.hl7.bean.PatientMsh;
import mx.gnk.hl7.bean.PatientNte;
import mx.gnk.hl7.bean.PatientOrc;
import mx.gnk.hl7.bean.PatientPid;
import mx.gnk.hl7.bean.PatientRxc;
import mx.gnk.hl7.bean.PatientRxd;
import mx.gnk.hl7.bean.PatientRxe;
import mx.gnk.hl7.bean.PatientRxr;
import mx.gnk.hl7.bean.PatientTq1;
import mx.gnk.hl7.bean.PatientVisit;
import mx.gnk.service.message.rde011message;
import mx.gnk.service.message.rds013message;
import mx.gnk.service.message.rreO12message;

/**
 *
 * @author jmejia
 */
public class ParseHl7Message implements Serializable{
    
    public Hl7Message parseMessage(RRE_O12 message){
        System.out.println("RRE_O12");
        System.out.println(message);
        Hl7Message messageRreO12 = new Hl7Message();
        // TODO get type of message
        //ORC NW o CA
        //NW nueva solicitud
        //CA cancelacion de solicitud
        //message.
        if(message!=null){
            // TODO notificacion de asistencia RDE_011
            rreO12message rreO12Message = new rreO12message();
            PatientMsh patientMsh = rreO12Message.getBeanPatientMshRREO12(message);
            AcuseMsa acuseMsa = rreO12Message.getMsaValueRRE012(message);
            messageRreO12.setAcuseMsa(acuseMsa);
            messageRreO12.setPatientMsh(patientMsh);
        }        
        return messageRreO12;
    }
    
    
    public Hl7Message parseMessage(RDE_O11 message){
        Hl7Message messageRde011 = new Hl7Message();
        //System.out.println("RDE_O11");
        //System.out.println(message);
        // TODO get type of message
        //ORC NW o CA
        //NW nueva solicitud
        //CA cancelacion de solicitud
        //message.
        if(message!=null){
            // TODO notificacion de asistencia RDE_011
            rde011message rde011Message = new rde011message();
            PatientMsh patientMsh = rde011Message.getBeanPatientMshRDE011(message);
            PatientPid patientPid = rde011Message.getBeanPatientRDE011(message);
            PatientVisit patientVisit = rde011Message.getBeanVisitPatientRDE011(message);
            PatientOrc patientOrc = rde011Message.getBeanOrderOrcRDE011(message);
            PatientRxe patientRxe = rde011Message.getBeanOrderRxeRDE011(message);
            PatientNte patientNte = rde011Message.getBeanOrderNteRDE011(message);
            PatientTq1 patientTq1 = rde011Message.getBeanOrderTq1RDE011(message);
            PatientRxr patientRxr = rde011Message.getBeanOrderRxrRDE011(message);
            PatientRxc patientRxc = rde011Message.getBeanOrderRxcRDE011(message);
            PatientFt1 patientFt1 = rde011Message.getBeanOrderFt1RDE011(message);
            
            messageRde011.setPatientPid(patientPid);
            messageRde011.setPatientVisit(patientVisit);
            messageRde011.setPatientOrc(patientOrc);
            messageRde011.setPatientRxe(patientRxe);
            messageRde011.setPatientNte(patientNte);
            messageRde011.setPatientTq1(patientTq1);
            messageRde011.setPatientRxr(patientRxr);
            messageRde011.setPatientRxc(patientRxc);
            messageRde011.setPatientFt1(patientFt1);
            messageRde011.setPatientMsh(patientMsh);
        }
        return messageRde011;    
    }
    
    public Hl7Message parseMessage(RDS_O13 message){
        System.out.println("RDS_O13");
        System.out.println(message);        
        rds013message rds013Message = new rds013message();
        Hl7Message messageRds013 = new Hl7Message();
        // TODO get type of message
        
        // GET MSH OBJECT 
        // GET RDS INFO PATIENT
        //      PATIENT VISIT
        PatientPid patientPid = rds013Message.getBeanPatientRDS013(message);
        
        PatientVisit patientVisit = rds013Message.getBeanVisitPatientRDS013(message);
        // GET RDS ORDER
        //      ORC
        PatientOrc patientOrc = rds013Message.getBeanOrderOrcRDS013(message);
        //      TIMING
        //      ORDER DETAIL
        //          ORDER DETAIL SUPLEMENT
        //      ENCODING
        //          RXE
        //          RXE.1
        //          RXE.2
        PatientRxe patientRxe = rds013Message.getBeanOrderRxeRDS013(message);        
        //      RXD
        PatientRxd patientRxd= rds013Message.getBeanOrderRxdRDS013(message);
        PatientMsh patientMsh= rds013Message.getBeanPatientMshRDSO13(message);
        
        messageRds013.setPatientPid(patientPid);
        messageRds013.setPatientVisit(patientVisit);
        messageRds013.setPatientOrc(patientOrc);
        messageRds013.setPatientRxe(patientRxe);
        messageRds013.setPatientRxd(patientRxd);
        messageRds013.setPatientMsh(patientMsh);
        
        return messageRds013;
    }

    public void ParseMessage(Object message) throws ClassNotFoundException{        
        if(message instanceof RDE_O11){
            parseMessage((RDE_O11) message);
        }else if(message instanceof RDS_O13){
            parseMessage((RDS_O13) message);
        }else{
            throw new ClassNotFoundException("Not found type of hl7 message: "+message.getClass().getName());
        }
    }
 
    public Hl7Message ParseMessage(Message message) throws ClassNotFoundException, HL7Exception{                
        Hl7Message hl7message = new Hl7Message();
        
        if(message instanceof RDE_O11){
            hl7message = parseMessage((RDE_O11) message);
        }else if(message instanceof RDS_O13){
            hl7message = parseMessage((RDS_O13) message);
        }else if(message instanceof RRE_O12){
            hl7message = parseMessage((RRE_O12) message);
        }else{
            //throw new ClassNotFoundException("Not found type of hl7 message: "+message.getClass().getName());
            hl7message=null;
        }
                
        return hl7message;
    }    
    
    public static void printXmlSchemaMessage(Message message) throws HL7Exception{
        //instantiate an XML parser 
        XMLParser xmlParser = new DefaultXMLParser();

        //encode message in XML 
        String ackMessageInXML = xmlParser.encode(message);

        //print XML-encoded message to standard out
        System.out.println(ackMessageInXML);                
    }
}
