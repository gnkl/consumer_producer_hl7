package mx.com.gnkl.demo;

import ca.uhn.hl7v2.parser.PipeParser;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import mx.gnk.hl7.bean.Hl7Message;
import org.springframework.stereotype.Service;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import mx.gnk.service.ParseHl7Message;
import ca.uhn.hl7v2.HL7Exception;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.net.ConnectException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gnkl.notificaciones.Aplicacion;
import mx.gnkl.notificaciones.Notificacion;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;

@Service
/**
 *
 * @author jmejia
 */
public class JmsMessageListener {

    private static final int MESSAGE_CONTROL_ID_LOCATION = 9;
    private static final String FIELD_DELIMITER = "|";
    //private Socket connection;
    private static final List pool = new LinkedList();
    static final char END_OF_BLOCK = '\u001c';
    static final char START_OF_BLOCK = '\u000b';
    static final char CARRIAGE_RETURN = 13;
    static final String appID = "87b602c3-d1c4-431c-9e30-806c951362b2";
    static final String appAUTH = "YjMwMmQyMzktY2E4OS00MGU4LWJkOWMtNTViODhjOTUwMjMy";

    //private static final String PATH_HL7_FILE="/var/log/gnk/hl7/";
    private static final String PATH_HL7_FILE = "";

    @Autowired
    public JmsMessageProducer producer;

    public Aplicacion app;

    public JmsMessageListener() {
        this.app = Aplicacion.crearAplicacion(appAUTH, appID);
    }

    public String handleMessage(String text) {
        Hl7Message hl7message = null;

        try {
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println(text);
            Gson gson = new GsonBuilder().create();
            hl7message = gson.fromJson(text, Hl7Message.class);
            System.out.println("hl7message: " + hl7message);
            System.out.println("cant total: " + hl7message.getPatientRxe().getCantidadTotal());

            String result = executeQueryPost(hl7message);

            if (result.equals("Internal Server Error") || result.equals("Not Found")) {
                System.out.println("--- Encolando a errores. ---");
                producer.sendText(text);
                sendNotificacion(hl7message.getPatientMsh().getIdUnidadMedica());
            }

            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("--- Encolando a errores. ---");
            producer.sendText(text);
            sendNotificacion(hl7message.getPatientMsh().getIdUnidadMedica());
        }

        return "ACK from handleMessage";
    }

    public Hl7Message parseHL7(String ackMessageString) throws IOException, ClassNotFoundException, HL7Exception {

        PipeParser pipeParser = new PipeParser();
        ParseHl7Message parseHl7Message = new ParseHl7Message();
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        String date = sdf.format(new Date());

        if (ackMessageString.contains("RDE_O11")) {
            File RDEO11_File = new File(PATH_HL7_FILE + "RDE_O11_" + date + ".log");
            FileUtils.writeStringToFile(RDEO11_File, ackMessageString, true);
        } else if (ackMessageString.contains("RDS_O13")) {
            File RDS_O13_File = new File(PATH_HL7_FILE + "RDS_O13_" + date + ".log");
            FileUtils.writeStringToFile(RDS_O13_File, ackMessageString, true);
        } else if (ackMessageString.contains("RRE_O12")) {
            File RRE_O12_File = new File(PATH_HL7_FILE + "RRE_O12_" + date + ".log");
            FileUtils.writeStringToFile(RRE_O12_File, ackMessageString, true);
        }
        Hl7Message messageHl7 = parseHl7Message.ParseMessage(pipeParser.parse(ackMessageString));
        return messageHl7;
    }

    public String sendHL7Message(mx.gnk.hl7.bean.Hl7Message hl7message, String message) {
        String messageControlID = getMessageControlID(message);
        if (hl7message != null && hl7message.getPatientMsh() != null) {
            String estructuraMensaje = hl7message.getPatientMsh().getEstructuraMensaje();
            String idUnidadMedica = hl7message.getPatientMsh().getIdUnidadMedica();
            if (estructuraMensaje.contains("RDE_O11") && hl7message.getPatientOrc().getOrderControl().equals("OK")) {
                return createHL7MessageACKRDEO11("", messageControlID);
            } else {
                String mensaje = "Mensaje recibido satisfactoriamente";
                return createHL7MessageRREO12(mensaje, messageControlID, "AA", idUnidadMedica);
            }
        } else {
            String mensaje = "Mensaje recibido";
            return createHL7MessageRREO12(mensaje, messageControlID, "AA", null);
        }
    }

    private String getMessageControlID(String aParsedHL7Message) {
        int fieldCount = 0;
        StringTokenizer tokenizer = new StringTokenizer(aParsedHL7Message, FIELD_DELIMITER);

        while (tokenizer.hasMoreElements()) {
            String token = tokenizer.nextToken();
            fieldCount++;
            if (fieldCount == MESSAGE_CONTROL_ID_LOCATION) {
                return token;
            }
        }

        return "";
    }

    public String createHL7MessageACKRDEO11(String message, String messageControlID) {
        Date date = new Date();
        String dateString = new SimpleDateFormat("yyyyMMddHHmmss").format(date);
        String md5MessageId = DigestUtils.md5Hex(String.valueOf(date));
        String mshMessage = "MSH|^~\\&|||||" + dateString + "||ACK^RDE_O11^ACK|" + md5MessageId + "|P|2.5||||AL|ER|ACK||P|2.2";
        String msaMessage = "MSA|AA|" + messageControlID;

        StringBuffer ackMessage = new StringBuffer();
        ackMessage = ackMessage.append(START_OF_BLOCK)
                .append(mshMessage)
                .append(CARRIAGE_RETURN)
                .append(msaMessage)
                .append(CARRIAGE_RETURN)
                .append(END_OF_BLOCK)
                .append(CARRIAGE_RETURN);

        return ackMessage.toString();
    }

    public String createHL7MessageRREO12(String message, String messageControlID, String tipo, String unidadMedica) {
        String codeId = "";
        if (tipo != null) {
            codeId = tipo;
        }

        Date date = new Date();
        String mshMessage = "";
        String dateString = new SimpleDateFormat("yyyyMMddHHmmss").format(date);

        if (unidadMedica != null) {
            //mshMessage = "MSH|^~\\&|EXT_SYS_RECETA|"+unidadMedica+"|ALERT|"+unidadMedica+"|"+dateString+"||RRE^O12^RRE_O12|1|P|2.5|||AL|ER|ACK||P|2.2";
            mshMessage = "MSH|^~\\&|EXT_SYS_RECETA|" + unidadMedica + "|ALERT|" + unidadMedica + "|" + dateString + "||RRE^O12^RRE_O12|1|P|2.5|||AL|ER";
        } else {
            //mshMessage = "MSH|^~\\&|EXT_SYS_RECETA||ALERT||"+dateString+"||RRE^O12^RRE_O12|1|P|2.5|||AL|ER|ACK||P|2.2";
            mshMessage = "MSH|^~\\&|EXT_SYS_RECETA||ALERT||" + dateString + "||RRE^O12^RRE_O12|1|P|2.5|||AL|ER";
        }

        String msaMessage = "MSA|" + codeId + "|" + messageControlID + "|" + message + "|";

        StringBuffer ackMessage = new StringBuffer();
        ackMessage = ackMessage.append(START_OF_BLOCK)
                .append(mshMessage)
                .append(CARRIAGE_RETURN)
                .append(msaMessage)
                .append(CARRIAGE_RETURN)
                .append(END_OF_BLOCK)
                .append(CARRIAGE_RETURN);

        return ackMessage.toString();
    }

    public String serialize(Object obj, boolean pretty) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        mapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);

        if (pretty) {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        }

        return mapper.writeValueAsString(obj);
    }

    public String executeQueryPost(Hl7Message messageHl7) throws IOException, ConnectException {
        String statusExec = "";
        String url = "";
        String IdUnidad = messageHl7.getPatientMsh().getIdUnidadMedica();

        switch (IdUnidad) {
            case "20001":
                url = "http://localhost:9190/SCR_HL7_PRUEBA/ProcessHl7Message";
                break;
            case "20010":
                url = "http://localhost:8080/SCR_HL7_HGTEXCOCO/ProcessHl7Message";
                break;
            case "20013":
                url = "http://localhost:8080/SCR_HL7_HGMAXIMILIANO/ProcessHl7Message";
                break;
            case "20014":
                url = "http://localhost:8080/SCR_HL7_HGATLACOMULCO/ProcessHl7Message";
                break;
            case "20016":
                url = "http://localhost:8080/SCR_HL7_HMILOSREYES/ProcessHl7Message";
                break;
            case "20017":
                url = "http://localhost:8080/SCR_HL7_HGTENANCINGO/ProcessHl7Message";
                break;
            case "20018":
                url = "http://localhost:8080/SCR_HL7_HMIJOSEFA/ProcessHl7Message";
                break;
            case "20020":
                url = "http://localhost:8080/SCR_HL7_HMICHIMALHUACAN/ProcessHl7Message";
                break;
            case "20026":
                url = "http://localhost:8080/SCR_HL7_HMCHICONCUAC/ProcessHl7Message";
                break;
            case "20028":
                url = "http://localhost:8080/SCR_HL7_HGVALENTINZENTLALPAN/ProcessHl7Message";
                break;
            case "20029":
                url = "http://localhost:8080/SCR_HL7_CADNAUCALPAN/ProcessHl7Message";
                break;

        }

        HttpClient client = new HttpClient();

        //Instantiate a GET HTTP method
        PostMethod method = new PostMethod(url);
        method.setRequestHeader("Content-type", "text/xml; charset=UTF-8");

        String messageSerialize = serialize(messageHl7, true);
        NameValuePair nvp1 = new NameValuePair("message", messageSerialize);

        method.setQueryString(new NameValuePair[]{nvp1});

        int statusCode = client.executeMethod(method);

        System.out.println("Status Code = " + statusCode);
        System.out.println("QueryString>>> " + method.getQueryString());
        System.out.println("Status Text>>>" + HttpStatus.getStatusText(statusCode));
        statusExec = HttpStatus.getStatusText(statusCode);

        return statusExec;
    }

    public void sendNotificacion(String id) {
        try {
            System.out.println("--- Enviando notificación. ---");
            Notificacion notificacion = new Notificacion("https://image.ibb.co/dA1eNQ/Bell_icon.png");
            notificacion.agregarMensaje(Notificacion.ESPAÑOL_IDIOMA, "Error en proceso HL7",
                    String.format("No se proceso correctamente un mensaje de la unidad %s", id));
            this.app.enviarMensaje(notificacion);
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }
}
